A collection of common settings I use accross the GNU/Linux boxes I use.

## Load files
To load the settings from an external file for the vimrc, bashrc, and screenrc
just add **source *external-filepath*** inside each of the files.
